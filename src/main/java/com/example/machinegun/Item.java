package com.example.machinegun;

public class Item {

    private String id;

    private int quantity;

    private String seller;

    public Item(String id, int quantity, String seller) {
        this.id = id;
        this.quantity = quantity;
        this.seller = seller;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }
}
