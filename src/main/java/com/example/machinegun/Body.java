package com.example.machinegun;

import java.util.ArrayList;
import java.util.List;

public class Body {

    private String postalCode;

    private String country;

    private List<Item> items = new ArrayList<>();

    public Body(Item item01) {
        items.add(item01);
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
