package com.example.machinegun;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ClientElastic {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientElastic.class);

    private String baseUrl = "https://2da562eed2c3404abaa456ec7a482e97.us-east-1.aws.found.io:9243/sc-seller/sc-seller/ebgames?pretty";

    private RestTemplate restTemplate = new RestTemplate();

    public void method() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
        Date initDate = new Date();
        Long millisInitDate = initDate.getTime();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic ZWxhc3RpYzpudFhlQWZ5bVN2NjhhREJLWGxHUHRUY3M=");

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.exchange(baseUrl, HttpMethod.GET, request, String.class);
        Date endDate = new Date();
        Long millisEndDate = endDate.getTime();
        //LOGGER.info((millisEndDate - millisInitDate) + " - " + simpleDateFormat.format(initDate) + "|" + simpleDateFormat.format(endDate) + " " + response.getStatusCodeValue() /*+ " " + response*/);
    }

}
