package com.example.machinegun;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ClientSellerCenter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientSellerCenter.class);

    private String baseUrl = "http://34.232.88.191:8080/sellercenter/marketplaces/%s/pvt/orderForms/simulation?sc=1&an=%s";

    private RestTemplate restTemplate = new RestTemplate();

    private Body body;

    public ClientSellerCenter(final String accountName, final String marketPlace, final Body body) {
        this.baseUrl = String.format(baseUrl, accountName, marketPlace);
        this.body = body;
    }

    public void method() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
        Date initDate = new Date();
        Long millisInitDate = initDate.getTime();
        HttpEntity<Body> request = new HttpEntity<>(body);
        String response = restTemplate.postForObject(baseUrl, request, String.class);
        Date endDate = new Date();
        Long millisEndDate = endDate.getTime();
        LOGGER.info((millisEndDate - millisInitDate) + " - " + simpleDateFormat.format(initDate) + "|" + simpleDateFormat.format(endDate) + " " + response);
    }

}
