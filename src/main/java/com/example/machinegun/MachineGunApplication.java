package com.example.machinegun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//@SpringBootApplication
public class MachineGunApplication {

    public static void main(String[] args) {
        //SpringApplication.run(MachineGunApplication.class, args);

        /*
        List<ClientSellerCenter> clientSellerCenters = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            clientSellerCenters.add(new ClientSellerCenter("ebgames", "plazaveaambienteqa", new Body(new Item("1597721001599", 1, "1"))));
        }*/

        List<ClientElastic> clientElastics = new ArrayList<>();
        for (int i = 0; i < Integer.valueOf(args[0]); i++) {
            clientElastics.add(new ClientElastic());
        }


        /*for (ClientSellerCenter clientSellerCenter : clientSellerCenters) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(() -> {
                clientSellerCenter.method();
            });
        }*/

        for (ClientElastic clientSellerCenter : clientElastics) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(() -> {
                clientSellerCenter.method();
            });
        }
    }
}
